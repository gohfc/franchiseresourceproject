﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FranchiseResource
{
    public partial class FindFe : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            divFindRes.Visible = false;
            bltFELinks.Items.Clear();
            ltlFEInfo.Text = "";
            ltlHead.Text = "Search Results";
            if (!Page.IsPostBack)
            {
                string strTCode = "";
                strTCode = Request.QueryString["TerrCode"];
                if (strTCode != null)
                {
                    btnSubmit_Click(sender, e);
                }
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (ddlSelectSite.SelectedValue == "") return;
            _FetchInfo();
        }

        private void _FetchInfo()
        {
            DataTable dtFEInfo = new DataTable();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["rplHFCResources"].ConnectionString);
            SqlCommand cmdSearch = new SqlCommand("prFEFinder", con);
            cmdSearch.Parameters.Clear();
            cmdSearch.Parameters.AddWithValue("@OutputBrand", ddlSelectSite.SelectedValue); //this.Session["Site"].ToString()
            cmdSearch.Parameters.AddWithValue("@MyBrand", (ddlMyBrand.SelectedValue.Trim() == string.Empty) ? null : ddlMyBrand.SelectedValue); //this.Session["MyBrand"].ToString()
            cmdSearch.Parameters.AddWithValue("@MyTerrCode", (txtTerrNum.Text.Trim() == string.Empty) ? null : txtTerrNum.Text);
            cmdSearch.Parameters.AddWithValue("@MyZip", (txtZip.Text.Trim() == string.Empty) ? null : txtZip.Text);
            cmdSearch.Parameters.AddWithValue("@OutputState", (txtState.Text.Trim() == string.Empty) ? null : txtState.Text);
            cmdSearch.Parameters.AddWithValue("@OutputPointOfContact", (txtName.Text.Trim() == string.Empty) ? null : txtName.Text);
            cmdSearch.CommandTimeout = 0;
            using (var da = new SqlDataAdapter(cmdSearch))
            {
                cmdSearch.CommandType = CommandType.StoredProcedure;
                da.Fill(dtFEInfo);
            }

            if (dtFEInfo.Rows.Count > 0)
            {
                ltlFEInfo.Text += "<div class='result-set-wrapper'>";
                foreach (DataRow dr in dtFEInfo.Rows)
                {
                    ltlFEInfo.Text += "<br /><strong>Brand:</strong> " + dr["Brand"];
                    ltlFEInfo.Text += "<br /><br /><strong>Zip Status:</strong> " + GetZipStatus(dr["Contract"].ToString());
                    ltlFEInfo.Text += "<br /><br /><strong>Territory Code:</strong> " + dr["TerrCode"].ToString();
                    ltlFEInfo.Text += "<br /><br /><strong>Territory Name:</strong> " + dr["TerrName"].ToString();
                    ltlFEInfo.Text += "<br /><br /><strong>Franchisee Name:</strong> " + dr["PointOfContact"].ToString();
                    ltlFEInfo.Text += "<br /><br /><strong>Phone Number:</strong> " + PhoneFormat(dr["Phone1"].ToString());
                    ltlFEInfo.Text += "<br /><br /><strong>Alternate Phone:</strong> " + PhoneFormat(dr["Phone2"].ToString());
                    ltlFEInfo.Text += "<br /><br /><strong>Email Address:</strong> " + dr["Email"].ToString();
                    ltlFEInfo.Text += "<br /><br /><div class='seperator'></div>";

                    divFindRes.Visible = true;
                }
                ltlFEInfo.Text += "</div>";
                ltlHead2.Text = "";
            }
            else
            {
                divFindRes.Visible = true;
                ltlHead2.Text = "No Results Found";
            }

        }

        private string GetZipStatus(string status)
        {
            string zipstatus = string.Empty;

            if (String.IsNullOrEmpty(status))
                return zipstatus;

            switch (status.ToLower())
            {
                case "c":
                    zipstatus = "Contract";
                    break;

                case "n":
                    zipstatus = "Non-contract/Gray Area";
                    break;
            }

            return zipstatus;
        }

        private string PhoneFormat(string phone)
        {
            if (String.IsNullOrEmpty(phone)) return "";

            if (phone.Length == 10)
                return phone = "(" + phone.Substring(0, 3) + ")" + phone.Substring(3, 3) + "-" + phone.Substring(6, 4);
            else if (phone.Length < 10) return "";
            else return phone;
        }

        protected void ddlSelectSite_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Session["Site"] = ddlSelectSite.SelectedValue;
        }
        protected void ddlMyBrand_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Session["MyBrand"] = ddlMyBrand.SelectedValue;
        }
    }
}