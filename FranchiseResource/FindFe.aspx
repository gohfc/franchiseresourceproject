﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FindFe.aspx.cs" Inherits="FranchiseResource.FindFe" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title></title>
	<meta http-equiv="refresh" content="1200">
    <link href="../Style/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="xsnazzy">
            <b class="xtop"><b class="xb0 color_yellow2"></b><b class="xb1 color_yellow2"></b><b
                class="xb2 color_yellow"></b><b class="xb3 color_yellow"></b><b class="xb4 color_yellow"></b></b>
            <div class="xboxcontent color_yellow3 main_bg_box2">
                <p class="image-wrapper">
                    <asp:Image ID="Image1" runat="server" ImageUrl="../Style/logo.png" /></p>
                <h1 class="color_yellow">Find a Franchise</h1>
				<div class="description">
                    <p>Find a Franchise by selecting a specific Brand or All Brands, add your Brand 
					and Territory number to get all of the other Franchisees within your gray and 
					contracted postal codes. You can also search by zip, State/Province or Franchisee name.</p>
                </div>
                <table class="input-wrapper">
                    <tr>
                        <td>&nbsp; </td>
                        <td><strong>Find By Brand:</strong></td>
                        <td>
                            <asp:DropDownList ID="ddlSelectSite" runat="server" AutoPostBack="true" EnableViewState="true"   OnSelectedIndexChanged="ddlSelectSite_SelectedIndexChanged">
                                <asp:ListItem Value="ALL">All Brands</asp:ListItem>
                                <asp:ListItem Value="AC">AdvantaClean</asp:ListItem>
                                <asp:ListItem Value="BB">Budget Blinds</asp:ListItem>
                                <asp:ListItem Value="BT">Bath Tune Up</asp:ListItem>
                                <asp:ListItem Value="CC">Concrete Craft</asp:ListItem>
                                <asp:ListItem Value="KT">Kitchen Tune Up</asp:ListItem>
                                <asp:ListItem Value="PG">Premiere Garage</asp:ListItem>
                                <asp:ListItem Value="TC">The Tailored Closet</asp:ListItem>
                                <asp:ListItem Value="TL">Tailored Living</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><strong></strong></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp; </td>
                        <td><strong>My Brand:</strong></td>
                        <td>
                            <asp:DropDownList ID="ddlMyBrand" runat="server" AutoPostBack="true" EnableViewState="true"   OnSelectedIndexChanged="ddlMyBrand_SelectedIndexChanged">
                                <asp:ListItem Value="">--Select A Brand--</asp:ListItem>
                                <asp:ListItem Value="AC">AdvantaClean</asp:ListItem>
                                <asp:ListItem Value="BB">Budget Blinds</asp:ListItem>
                                <asp:ListItem Value="BT">Bath Tune Up</asp:ListItem>
                                <asp:ListItem Value="CC">Concrete Craft</asp:ListItem>
                                <asp:ListItem Value="KT">Kitchen Tune Up</asp:ListItem>
                                <asp:ListItem Value="PG">Premiere Garage</asp:ListItem>
                                <asp:ListItem Value="TC">The Tailored Closet</asp:ListItem>
                                <asp:ListItem Value="TL">Tailored Living</asp:ListItem>
                            </asp:DropDownList>
                            <strong class="territory-field">My Territory Number:</strong><asp:TextBox ID="txtTerrNum" runat="server" Width="100px" MaxLength="10"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>&nbsp; </td>
                        <td><strong>Zip Code:</strong></td>
                        <td>
                            <asp:TextBox ID="txtZip" runat="server" Width="100px" MaxLength="10"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>&nbsp; </td>
                        <td><strong>State/Province:</strong></td>
                        <td>
                            <asp:TextBox ID="txtState" runat="server" MaxLength="2" Width="100px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>&nbsp; </td>
                        <td><strong>Franchisee Name:</strong></td>
                        <td>
                            <asp:TextBox ID="txtName" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>&nbsp; </td>
                        <td></td>
                        <td>
                            <asp:Button ID="btnSubmit" Text="Search" runat="server" OnClick="btnSubmit_Click" /></td>
                    </tr>
                </table>
            </div>
            <b class="xbottom"><b class="xb4 color_yellow"></b><b class="xb3 color_yellow"></b><b
                class="xb2 color_yellow"></b><b class="xb1 color_yellow2"></b><b class="xb0 color_yellow2"></b></b>
        </div>
        <div class="xsnazzy" id="divFindRes" runat="server">
            <b class="xtop"><b class="xb0 color_green2"></b><b class="xb1 color_green2"></b><b class="xb2 color_green"></b><b class="xb3 color_green"></b><b class="xb4 color_green"></b></b>
            <div class="xboxcontent color_green3 main_bg_box2">
                <h1 class="color_green">
                    <asp:Literal ID="ltlHead" runat="server" Text=""></asp:Literal></h1>
                <h2>
                    <asp:Literal ID="ltlHead2" runat="server"></asp:Literal></h2>
                <asp:Literal ID="ltlFEInfo" runat="server"></asp:Literal>
                <asp:BulletedList ID="bltFELinks" DisplayMode="HyperLink" runat="server"></asp:BulletedList>
            </div>
            <b class="xbottom"><b class="xb4 color_green"></b><b class="xb3 color_green"></b><b class="xb2 color_green"></b><b class="xb1 color_green2"></b><b class="xb0 color_green2"></b></b>
        </div>
        
    </form>
</body>
</html>

